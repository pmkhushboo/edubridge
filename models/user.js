const mongoose = require("mongoose");
const dataTables = require('mongoose-datatables')
const Schema = mongoose.Schema;

const userSchema = new Schema({
    name: String,
    email: String,
    contact: Number,
    profile_picture: {
        data: Buffer,
        // image: Buffer,
        contentType: String
    },
    address: String,
    company: String,
    createdAt: {
        type: Date,
        default: Date.now,
    },
}, {
    versionKey: false
});

userSchema.plugin(dataTables);

module.exports = mongoose.model("User", userSchema);
