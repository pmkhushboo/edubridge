const fs = require('fs');
const userService = require("./services/userService");
const connectDB = require("./database/connection");
connectDB();
// JSON data
const user = require('./models/user');
// Build paths
const { buildPathHtml } = require('./buildPaths');

/**
 * Take an object which has the following model
 * @param {Object} item 
 * @model
 * {
 * name: String,
 * email: String,
 * contact: Number,
 * profile_picture: {
 *   data: Buffer,
 *   contentType: String
 * },
 * address: String,
 * company: String,
 * }
 * 
 * @returns {String}
    <td>${item.profile_picture}</td>
 */
const createRow = (item) => `
  <tr>
    <td>${item.name}</td>
    <td>${item.email}</td>
    <td>${item.contact}</td>
    <td>${item.address}</td>
    <td>${item.company}</td>
  </tr>
`;

/**
 * @description Generates an `html` table with all the table rows
 * @param {String} rows
 * @returns {String}
        <th>Profile Picture</td>
 */
const createTable = (rows) => `
  <table>
    <tr>
        <th>Name</td>
        <th>Email</td>
        <th>Contact</td>
        <th>Address</td>
        <th>Comapny</td>
    </tr>
    ${rows}
  </table>
`;

/**
 * @description Generate an `html` page with a populated table
 * @param {String} table
 * @returns {String}
 */
const createHtml = (table) => `
  <html>
    <head>
      <style>
        table {
          width: 100%;
        }
        tr {
          text-align: left;
          border: 1px solid black;
        }
        th, td {
          padding: 15px;
        }
        tr:nth-child(odd) {
          background: #CCC
        }
        tr:nth-child(even) {
          background: #FFF
        }
        .no-content {
          background-color: red;
        }
      </style>
    </head>
    <body>
      ${table}
    </body>
  </html>
`;

/**
 * @description this method takes in a path as a string & returns true/false
 * as to if the specified file path exists in the system or not.
 * @param {String} filePath 
 * @returns {Boolean}
 */
const doesFileExist = (filePath) => {
    try {
        fs.statSync(filePath); // get information of the specified file path.
        return true;
    } catch (error) {
        return false;
    }
};

try {
    /* Check if the file for `html` build exists in system or not */
    if (doesFileExist(buildPathHtml)) {
        console.log('Deleting old build file');
        /* If the file exists delete the file from system */
        fs.unlinkSync(buildPathHtml);
    }

    var data;
    const generateData = new Promise(async (resolve, reject) => {
        const users = await userService.getAllUsersAsArray();
        if (users.length) {
            resolve(users);
        }
        reject(new Error('No Records'));
    }).then((userData) => {
        data = userData;
        /* generate rows */
        const rows = data.map(createRow).join('');
        /* generate table */
        const table = createTable(rows);
        /* generate html */
        const html = createHtml(table);
        /* write the generated html to file */
        fs.writeFileSync(buildPathHtml, html);
        console.log('Succesfully created an HTML table');
    }).catch(err => console.log(err));

} catch (error) {
    console.log('Error generating table', error);
}