const express = require("express");
const bodyParser = require('body-parser');
const { exec } = require("child_process");

const connectDB = require("./database/connection");
const userRouter = require("./routes/userRoutes");

const app = express();
const PORT = process.env.PORT || 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(__dirname + '/views'));
app.use(express.static(__dirname + '/downloads'));

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
// Set EJS as templating engine
app.set("view engine", "ejs");
app.engine('html', require('ejs').renderFile);

app.use("/api/users", userRouter);


var multer = require('multer');
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'uploads')
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now())
    }
});

var upload = multer({ storage: storage });


const userModel = require("./models/user");
var fs = require('fs');
var path = require('path');

app.get('/downloadPdf', (req, res) => {
    new Promise(async (resolve, reject) => {
        exec("node createPdf.js", (error, stdout, stderr) => {
            if (error) {
                console.log(`error: ${error.message}`);
                return;
            }
            if (stderr) {
                console.log(`stderr: ${stderr}`);
                return;
            }
            console.log(`stdout: ${stdout}`);
            resolve();
        });
    }).then(() => {
        const file = `${__dirname}/downloads/build.pdf`;
        res.download(file);
    });
});

app.post('/api/users', upload.single('profile_picture'), async (req, res, next) => {
    var obj = {
        name: req.body.name,
        email: req.body.email,
        contact: req.body.contact,
        address: req.body.address,
        company: req.body.company,
        profile_picture: {
            data: fs.readFileSync(path.join(__dirname + '/uploads/' + req.file.filename)),
            contentType: 'image/png'
        },
    }

    userModel.create(obj, (err, item) => {
        if (err) {
            console.log(err);
        }
        else {
            res.redirect('/');
        }
    });
});

connectDB();

app.listen(PORT, () => {
    console.log(`Example app listening on port ${PORT}`)
});