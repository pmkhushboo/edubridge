const fs = require('fs');
const puppeteer = require('puppeteer');
// Build paths
const { buildPathHtml, buildPathPdf } = require('./buildPaths');

const printPdf = async () => {
    console.log('Starting: Generating PDF Process, Kindly wait ..');
    /** Launch a headleass browser */
    const browser = await puppeteer.launch();
    /* 1- Ccreate a newPage() object. It is created in default browser context. */
    const page = await browser.newPage();
    /* 2- Will open our generated `.html` file in the new Page instance. */
    await page.goto(buildPathHtml, { waitUntil: 'networkidle0' });
    /* 3- Take a snapshot of the PDF */

    // declare html markup for header
    htmlHeader = `<div style="font-size: 15px; padding-top: 8px; text-align: center; width: 100%;">
            <span>USER LIST</span></span>
          </div>
        `;
    // declare html markup for footer
    htmlFooter = `<div style="font-size: 8px; padding-bottom: 8px; text-align: center; width: 100%;">
                <span style="padding-left: 20px; float:left">-Made By EduBridge</span><span style="padding-right: 20px; float:right">-Page<span class="pageNumber"></span></span>
              </div>
            `;

    // await page.emulateMedia('screen');            // use screen media
    const pdf = await page.pdf({
        landscape: true,
        // format: 'A4',
        displayHeaderFooter: true,
        headerTemplate: htmlHeader,
        footerTemplate: htmlFooter,
        margin: {
            top: '80px',
            right: '20px',
            bottom: '80px',
            left: '20px'
        }
    });
    /* 4- Cleanup: close browser. */
    await browser.close();
    console.log('Ending: Generating PDF Process');
    return pdf;
};

const init = async () => {
    try {
        const pdf = await printPdf();
        fs.writeFileSync(buildPathPdf, pdf);
        console.log('Succesfully created an PDF table');
    } catch (error) {
        console.log('Error generating PDF', error);
    }
};

init();