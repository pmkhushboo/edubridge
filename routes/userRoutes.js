const express = require("express");
const {
    getAllUsers,
    createUser,
    getUserById,
    updateUser,
    deleteUser,
    getUserTable,
    getAllUsersAsArray,
} = require("../controllers/userController");

const router = express.Router();

// router.route("/").get(getAllUsers);//.post(createUser);
router.route("/:id").get(getUserById).put(updateUser).delete(deleteUser);
router.route("/userDetailsTable").post(getUserTable);

module.exports = router;