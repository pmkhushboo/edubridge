const mongoose = require("mongoose");

mongoose.set('strictQuery', true);

const mongoDB = process.env.MONGODB_URI || 'mongodb://127.0.0.1:27017/finalDB';//localhost

const connectDB = async () => {
    try {
        // mongodb connection string
        const con = await mongoose.connect(mongoDB);

        console.log(`Mongodb connected:${con.connection.host}`);
    } catch (err) {
        console.log(err);
        process.exit(1);
    }
};

module.exports = connectDB;