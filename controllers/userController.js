const userService = require("../services/userService");
const userModel = require("../models/user");
const fs = require('fs');
const path = require('path');
const { exec } = require("child_process");

exports.getAllUsers = async (req, res) => {
    try {
        const users = await userService.getAllUsers();
        res.json({ data: users, status: "success" });
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
};

exports.createUser = async (req, res) => {
    try {
        // validate request
        if (!req.body) {
            res.status(400).send({ message: "Content can not be emtpy!" });
            return;
        }
        var obj = {
            name: req.body.name,
            email: req.body.email,
            contact: req.body.contact,
            address: req.body.address,
            company: req.body.company,
            profile_picture: {
                data: fs.readFileSync(path.join(__dirname + '/uploads/' + req.file.filename)),
                contentType: 'image/png'
            }
        }
        const user = await userService.createUser(obj);
        res.redirect("/index.html");
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
};

exports.getUserById = async (req, res) => {
    try {
        const user = await userService.getUserById(req.params.id);
        res.json({ data: user, status: "success" });
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
};

exports.updateUser = async (req, res) => {
    try {
        const user = await userService.updateUser(req.params.id, req.body);
        res.json({ data: user, status: "success" });
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
};

exports.deleteUser = async (req, res) => {
    try {
        const user = await userService.deleteUser(req.params.id);
        res.json({ data: user, status: "success" });
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
};

exports.getUserTable = async (req, res) => {
    try {
        const users = await userService.getAllUsers();
        if (!users.length) {
            var data = []
            for (var i = 1; i <= 20; i++) {
                data.push({
                    name: 'User ' + i,
                    email: 'User' + i + '@gmail.com',
                    contact: 'User ' + i,
                    profile_picture: 'User ' + i,
                    address: 'User ' + i,
                    company: 'User ' + i,
                });
            };
            userModel.create(data, function (err) { console.log(err) });
        }

        userModel.dataTables({
            limit: req.body.length,
            skip: req.body.start,
            order: req.body.order,
            columns: req.body.columns,
            search: {
                value: req.body.search.value,
                fields: ['name', 'email', 'address', 'company']
            },
        }).then(function (table) {
            //Generating build.html for PDF generation
            exec("node createTable.js", (error, stdout, stderr) => {
                if (error) {
                    console.log(`error: ${error.message}`);
                    // return;
                    // reject(new Error(`error: ${error.message}`));
                }
                if (stderr) {
                    console.log(`stderr: ${stderr}`);
                    // return;
                    // reject(new Error(`stderr: ${stderr}`));
                }
                console.log(`stdout: ${stdout}`);
            });
            //Sending result as json to datatable in index.html as ajax response
            res.json({
                data: table.data,
                recordsFiltered: table.total,
                recordsTotal: table.total
            });
        })
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
};